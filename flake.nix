{
  description = "HenryZeng 's NixOS Flake";

  inputs = {
    # flake inputs 有很多种引用方式，应用最广泛的格式是:
    #     github:owner/name/reference
    # 即 github 仓库地址 + branch/commit-id/tag

    # NixOS 官方软件源，这里使用 nixos-23.11 分支
    nixpkgs.url = "https://mirrors.ustc.edu.cn/nix-channels/nixos-23.11/nixexprs.tar.xz";
    nixpkgs-unstable.url = "https://mirrors.ustc.edu.cn/nix-channels/nixpkgs-unstable/nixexprs.tar.xz";

    home-manager = {
      url = "https://gitcode.net/HenryZeng_Zero/home-manager-mirror/-/archive/release-23.11/home-manager-mirror-release-23.11.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, home-manager, ... }@inputs: {
    nixosConfigurations = {
        "henryzeng-nixos" = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          specialArgs = { inherit inputs; }; # this is the important part
          modules = [
            ./configuration.nix
            ./hardware-configuration.nix
            ./programs.nix
            ./language.nix
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.henryzeng = import ./home.nix;
              home-manager.extraSpecialArgs = inputs;
            }
          ];
      };
    };
  };
}
